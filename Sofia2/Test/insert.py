#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-
import json, socket, time, sys
from collections import OrderedDict

arg1 = sys.argv[1]
#arg2 = sys.argv[2]
servidor = "127.0.0.1"
puerto = 8888
instancias = json.loads(open(arg1).read(), object_pairs_hook=OrderedDict)
#time.sleep(3)   
#print(instancias)
#print('\n')
for i in range(len(instancias)):
    cliente = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    cliente.connect((servidor, puerto))    
    time.sleep(1)
    mensaje = json.dumps(instancias[i])
#    print('Enviar mensaje')
#    print(mensaje)
#    print('\n')
    cliente.send(mensaje.replace("u\'", '\'').encode('utf8'))
    #print('recibiendo respuesta')
    respuesta = cliente.recv(4096)
    #print("[*] Respuesta recibida: "+respuesta.decode('utf8'))
    cliente.close()

'''cliente = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
cliente.connect((servidor, puerto))
time.sleep(6)
respuesta = cliente.recv(4096)
cliente.close()'''
