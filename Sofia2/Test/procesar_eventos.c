#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include "../Ficheros_Generados/Ontologia/ontologia.h"

int main(int argc, char *argv[]){

    int socket_desc , client_sock , c , read_size, sel;
    struct sockaddr_in server , client;
    char client_message[2000];
    char response[4096];
    struct timeval tv;
    fd_set readfds;
    const char *HTTPresponse = "HTTP/1.1 201 Created\r\n\r\n"; //Content-type: none\r\n\r\n
    Lectura_Termostato_t datos[40];
	Lectura_Termostato_t lt;
    Lectura_Termostato_t resp[40];
    int num_datos = 0;
    int num_resp = 0;
    unsigned int pos;
    float temperatura;
    int valido = 0, valido2 = 0;
    int i;
    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        return 1;
    }
     
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons( 8888 );

    tv.tv_sec = 5;
    tv.tv_usec = 0;
     
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        return 1;
    }
    //Listen
    listen(socket_desc , 10);
     
    //Accept and incoming connection
    c = sizeof(struct sockaddr_in);
     
    //accept connection from an incoming client
    while (1) {
        FD_ZERO(&readfds);
        FD_SET(socket_desc, &readfds);

        sel = select(socket_desc + 1, &readfds, NULL, NULL, &tv);

        if (sel <= 0){
            write(client_sock , "TIMEOUT", strlen("TIMEOUT"));
            close(client_sock);
            break;
        }else{
            client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
            if (client_sock < 0){
                return 1;
            }
            //Receive a message from client
            read_size = recv(client_sock , client_message , 2000 , 0);
            //Send the message back to client
            if(read_size == 0){
                fflush(stdout);
            }else if(read_size == -1){

            }
            else{
                pos = 0;
                valido2 = RecibirEvento(client_message, &lt);
                printf("datos obtenidos: temp -> %.2f, hum -> %d", lt.temp, lt.hum);
                num_datos = num_datos +1;
                if (valido2 == 0){
                    valido = ConsumirEvento(lt, &temperatura);
                    if (valido == 0) {
                        printf("Temperatura: %.2f\n", temperatura);
                        resp[num_resp] = lt;
                        num_resp = num_resp + 1;
                    }
                }
                write(client_sock , HTTPresponse, strlen(HTTPresponse));
                close(client_sock);
                strcpy(client_message, "");
            }
        }
    }
    close(socket_desc);
    printf("El numero total de aceptados es: %d\n", num_resp);
    return 0;
}

