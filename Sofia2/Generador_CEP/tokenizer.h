#ifndef _SOFIA_TOKENIZER_
#define _SOFIA_TOKENIZER_

#define MAX_TOKEN_LEN 64
#define _MIN(A,B) (((A)<(B))?(A):(B))

#define FIN_CADENA 0

typedef enum {TK_ABRE_CORCHETE,TK_CIERRA_CORCHETE,TK_ABRE_LLAVE,TK_CIERRA_LLAVE,TK_DOS_PUNTOS,TK_COMA,TK_ID,TK_NUM,TK_EOF,TK_MAX_LEN,TK_ERROR} token_t;

//char skip_spaces(char *ptr, unsigned int idx, unsigned char *npos);
//token_t next_token(char *ptr,char token[MAX_TOKEN_LEN+1],unsigned int idx, unsigned char *npos);

extern unsigned int idx;
extern const char field_names[8][MAX_TOKEN_LEN+1];



char skip_spaces(char *ptr);
token_t next_token(char *ptr,char token[MAX_TOKEN_LEN+1]);

int check_str(char str1[MAX_TOKEN_LEN+1], const char str2[MAX_TOKEN_LEN+1], volatile int len);
unsigned char get_value_date(char *str1, char str2[MAX_TOKEN_LEN+1], unsigned char idx);

#endif
