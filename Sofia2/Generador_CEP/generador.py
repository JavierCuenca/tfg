#!/usr/bin/python3
#Almacenar la SQL en una variable e ir jugando con ello.
import json, socket
import sys
from antlr4 import *
from sqlLexer import sqlLexer
from sqlParser import sqlParser
from sqlVisitor import sqlVisitor
from sqlListener import sqlListener
from sqlEvalListener import sqlListenerEval
from sqlEvalVisitor import sqlVisitorEval
from pprint import pprint
import codecs

'''******************* METODOS PARA LA CREACION DEL FICHERO *******************'''
#Metodo para obtener los datos de los mensajes.
def eventos_recibidos(funciones, cabecera, keys, tipos, ontologia):
    cabecera.write('int RecibirEvento(char *ptr,' + ontologia + '_t *instancia);' + '\n\n')
    funciones.write('#include <stdio.h>' + '\n')
    funciones.write('#include <stdlib.h>' + '\n')
    funciones.write('#include "tokenizer.h"' + '\n')
    funciones.write('#include "ontologia.h"' + '\n\n')
    funciones.write('#define _DEBUG' + '\n\n')
    funciones.write('typedef enum {ST_ESPERA,ST_ID,ST_ID_ATTR_OID,ST_FIN_ID,ST_CONTEXT_DATA,ST_CONTEXT_DATA_ATRIBUTOS,ST_CONTEXT_ATRIBUTO,ST_FIN_CONTEXT_DATA,ST_KP_INSTANCE, ST_FIN_KP_INSTANCE,ST_TIME_STAMP,ST_FIN_TIME_STAMP,ST_ONTOLOGIA,ST_ONTOLOGIA_ATRIBUTOS,')
    for i in range(len(keys)):
        funciones.write('ST_ONTOLOGIA_ATTR_' + keys[i].upper() + ',')
    funciones.write('ST_FIN_ONTOLOGIA,ST_FIN_EVENTO,ST_EVENTO,ST_ERROR} state_t;' + '\n\n')
    aux_fic = open("./Funciones_Defines.c" , "r")
    funciones.write(aux_fic.read())
    aux_fic.close()
    funciones.write("char *ontologia = \""+ ontologia[0] +'\";' + '\n\n')
    funciones.write('int RecibirEvento(char *ptr,' + ontologia + '_t *instancia){' + '\n')
    aux_fic = open("./Funcion_get_datos.c" , "r")
    funciones.write(aux_fic.read())
    aux_fic.close()
    funciones.write('\t\t' + 'case ST_ONTOLOGIA:' + '\n')
    funciones.write('\t\t\t' + 'CONSUMIR_OBJ("' + ontologia + '",' + str(len(ontologia)) +',ST_ONTOLOGIA_ATRIBUTOS);' + '\n')
    funciones.write('\t\t\t' + 'break;' + '\n')
    funciones.write('\t\t' + 'case ST_ONTOLOGIA_ATRIBUTOS:' + '\n')
    funciones.write('\t\t\t' + 'next_st = ST_ERROR;' + '\n')
    funciones.write('\t\t\t' + 'tk1 = next_token(ptr,attr);' + '\n')
    funciones.write('\t\t\t' + 'if ( tk1 == TK_ID) {' + '\n')
    funciones.write('\t\t\t\t' + 'tk2 = next_token(ptr,trash1);' + '\n')
    for i in range(len(keys)):
        if i == 0:
            funciones.write('\t\t\t\t' + 'if ( check_str(attr,"' + keys[i] + '",' + str(len(keys[i])) + ') && (tk2 == TK_DOS_PUNTOS)) {' + '\n')
        else:
            funciones.write('\t\t\t\t' + '} else if (check_str(attr,"'+ keys[i] + '",' + str(len(keys[i])) +') && (tk2 == TK_DOS_PUNTOS)) {' + '\n')
        funciones.write('\t\t\t\t\t' + 'next_st = ST_ONTOLOGIA_ATTR_' + keys[i].upper() + ';' + '\n')
    funciones.write('\t\t\t\t' + '}' + '\n')
    funciones.write('\t\t\t' + '} else if (tk1 == TK_COMA) {' + '\n')
    funciones.write('\t\t\t\t' + 'next_st = ST_ONTOLOGIA_ATRIBUTOS;' + '\n')
    funciones.write('\t\t\t' + '} else if (tk1 == TK_CIERRA_LLAVE) {' + '\n')
    funciones.write('\t\t\t\t' + 'next_st = ST_FIN_EVENTO;' + '\n')
    funciones.write('\t\t\t' + '}' + '\n')
    funciones.write('\t\t\t' + 'break;' + '\n')
    for i in range(len(keys)):
        funciones.write('\t\t' + 'case ST_ONTOLOGIA_ATTR_' + keys[i].upper() + ':' + '\n')
        if tipos[i] == 'int' :
            funciones.write('\t\t\t\t' + 'CONSUMIR_ATRB_ONTOLOGIA_INT(instancia->' + keys[i] + ');' + '\n')
        elif tipos[i] == 'float' :
            funciones.write('\t\t\t\t' + 'CONSUMIR_ATRB_ONTOLOGIA_FLOAT(instancia->' + keys[i] + ');' + '\n')
        funciones.write('\t\t\t' + 'break;' + '\n')
    aux_fic = open("./Fin_get_datos.c" , "r")
    funciones.write(aux_fic.read())
    aux_fic.close()
    funciones.write('\n')

#obtiene los campos que tiene que recoger la aplicacion
def campos_usados(keys):
    valores = '['
    for i in range(len(keys)):
        if i == len(keys) -1:
            valores += '\"' + keys[i] + '\"]'
        else:
            valores += '\"' + keys[i] + '\", '
    return valores

#Creacion de la estructura con los campos que contiene la ontologia
def crea_estructura(cabecera, tipo_campos, campos, ontologia):
    cabecera.write("typedef struct {" + '\n')
    for i in range(len(campos)):
        cabecera.write('\t' + str(tipo_campos[i]) + ' ' + campos[i] + ';' + '\n')
    cabecera.write('\t' + 'unsigned short year;' + '\n')
    cabecera.write('\t' + 'unsigned char month;' + '\n')
    cabecera.write('\t' + 'unsigned char day;' + '\n')
    cabecera.write('\t' + 'unsigned char hour;' + '\n')
    cabecera.write('\t' + 'unsigned char minutes;' + '\n')
    cabecera.write('\t' + 'unsigned char seconds;' + '\n')
    cabecera.write('\t' + 'unsigned short miliseconds;' + '\n')
    cabecera.write('\t' + 'char kpinstance[MAX_KP_INSTANCE_NAME];'+ '\n')
    cabecera.write("} " + ontologia + "_t;" + '\n\n')

# Función para sacar el tipo de datos del que se trata.
def tipo_dato(tipo): 
    if tipo == 'integer' :
        return "int"
    elif tipo == 'string' :
        return "char *"
    elif tipo == 'number' :
        return "float"

#Función para obtener los datos de la fecha. Esta funcion la tengo que retocar para que no se use el strcpy ni el strtok. No se si el strstr me 
#deja seguir usandolo. Supongo que no.

#Funcion con la que se genera la respuesta
def generar_respuesta(maximo, minimo, media, suma, camposselect, todo, ventana, campos):
    funcion = ''
    if len(camposselect) > 0:
        #print("campos" + str(camposselect))
        #print("longitud " + str(len(camposselect)))
        for i in range(len(camposselect)):
            funcion += '\t\t' + '*' + camposselect[i] + ' = a.' + camposselect[i] + ';' + '\n'
    else:
        funcion += '\t\t' + 'dat_calc[pos_ventana] = a;' + '\n'
        funcion += '\t\t' + 'if (pos_ventana == tam_ventana-1){ ' + '\n'
        funcion += '\t\t\t' + 'pos_ventana = 0;' + '\n'
        funcion += '\t\t' + '} else {' + '\n'
        funcion += '\t\t\t' + 'pos_ventana += 1;' + '\n'
        funcion += '\t\t' + '}' + '\n'
        funcion += '\t\t' + 'if (relleno_ventana == tam_ventana - 1){ ' + '\n'
        funcion += '\t\t\t' + 'for (i = 0; i<= relleno_ventana; i++){' + '\n'
        if len(maximo) > 0:
            #print("campos maximo " + str(maximo))
            #print("longitud maximo " + str(len(maximo)))
            for i in range(len(maximo)):
                funcion += '\t\t\t\t' + 'if (maxaux_' + maximo[i] + ' < dat_calc[i].' + maximo[i] + '){' + '\n'
                funcion += '\t\t\t\t\t' + 'maxaux_' + maximo[i] + ' = dat_calc[i].' + maximo[i] + ';' + '\n'
                funcion += '\t\t\t\t' + '}' + '\n'
                funcion += '\t\t\t\t' + 'if (i == relleno_ventana){' + '\n'
                funcion += '\t\t\t\t\t' +  '*max_' + maximo[i] + ' =  maxaux_' + maximo[i] + ';' + '\n'
                funcion += '\t\t\t\t' + '}' + '\n'
        if len(minimo) > 0:
            #print("campos minimo " + str(minimo))
            #print("longitud minimo " + str(len(minimo)))
            for i in range(len(minimo)):
                funcion += '\t\t\t\t' + 'if (minaux_' + minimo[i] + ' > dat_calc[i].' + minimo[i] + '){' + '\n'
                funcion += '\t\t\t\t\t' +  'minaux_' + minimo[i] + ' =  dat_calc[i].' + minimo[i] + ';' + '\n'
                funcion += '\t\t\t\t' + '}' + '\n'
                funcion += '\t\t\t\t' + 'if (i == relleno_ventana){' + '\n'
                funcion += '\t\t\t\t\t' +  '*min_' + minimo[i] + ' =  minaux_' + minimo[i] + ';' + '\n'
                funcion += '\t\t\t\t' + '}' + '\n'
        if len(media) > 0:
            #print("campos media " + str(media))
            #print("longitud media " + str(len(media)))
            for i in range(len(media)):
                funcion += '\t\t\t\t' + 'if (i == relleno_ventana){' + '\n'
                funcion += '\t\t\t\t\t' + 'avgaux_' + media[i] + ' = avgaux_' + media[i] + ' + dat_calc[i].' + media[i] + ';' + '\n'
                funcion += '\t\t\t\t\t' + 'avgaux_' + media[i] + ' = avgaux_' + media[i] + ' / tam_ventana;' + '\n'
                funcion += '\t\t\t\t\t' + '*avg_' + media[i] + ' = avgaux_' + media[i] + ';' + '\n'
                funcion += '\t\t\t\t' + '}else {' + '\n'
                funcion += '\t\t\t\t\t' + 'avgaux_' + media[i] + ' = avgaux_' + media[i] + ' + dat_calc[i].' + media[i] + ';' + '\n'
                funcion += '\t\t\t\t' + '}' + '\n'
        if len(suma) > 0:
            #print("campos suma " + str(suma))
            #print("longitud suma " + str(len(suma)))
            for i in range(len(suma)):
                funcion += '\t\t\t\t' + 'if (i == relleno_ventana){' + '\n'
                funcion += '\t\t\t\t\t' + 'sumaux_' + suma[i] + ' = sumaux_' + suma[i] + ' + dat_calc[i].' + suma[i] + ';' + '\n'
                funcion += '\t\t\t\t\t' + '*sum_' + suma[i] + ' = sumaux_' + suma[i] + ';' + '\n'
                funcion += '\t\t\t\t' + '}else {' + '\n'
                funcion += '\t\t\t\t\t' + 'sumaux_' + suma[i] + ' = sumaux_' + suma[i] + ' + dat_calc[i].' + suma[i] + ';' + '\n'
                funcion += '\t\t\t\t' + '}' + '\n'
        funcion += '\t\t\t' + '}' + '\n'
        funcion += '\t\t' + '} else {' + '\n'
        funcion += '\t\t\t' + 'relleno_ventana += 1;' + '\n'
        funcion += '\t\t\t' + 'return 2;' + '\n'
        funcion += '\t\t' + '}' + '\n'
    #print(funcion)
    return funcion

def define_consumir_eventos(camposselect, maximo, minimo, media, suma, todo, tipos, campos, ontologia):
    nombre_funcion = 'int ConsumirEvento('+ ontologia +'_t a'
    if todo == 0 :
        if len(camposselect) > 0 :
            for i in range(len(camposselect)):
                nombre_funcion += ', ' + tipos[campos.index(camposselect[i])] + ' *' + camposselect[i] + ' '
        if len(maximo) > 0:
            for i in range(len(maximo)):
                nombre_funcion += ', ' + tipos[campos.index(maximo[i])] + ' *max_' + maximo[i] + ' '
        if len(minimo) > 0:
            for i in range(len(minimo)):
                nombre_funcion += ', ' + tipos[campos.index(minimo[i])] + ' *min_' + minimo[i] + ' '
        if len(media) > 0:
            for i in range(len(media)):
                nombre_funcion += ', ' + tipos[campos.index(media[i])] + ' *avg_' + media[i] + ' '
        if len(maximo) > 0:
            for i in range(len(suma)):
                nombre_funcion += ', ' + tipos[campos.index(suma[i])] + ' *sum_' + suma[i] + ' '
    nombre_funcion += ')'
    return nombre_funcion

#Función para generar las variables que se necesitan para devolver los resultados de la media, suma, masimo y minimo.
def generar_variables_funciones(maximo, minimo, media, suma, tipos, campos, ventana, ontologia):
    variables = '\t'+'static '+ ontologia + '_t dat_calc['+ str(ventana) + '];' + '\n'
    variables += '\t'+'int tam_ventana = ' + str(ventana) +', i = 0;' + '\n'
    variables += '\t'+'static int pos_ventana = 0;' + '\n'
    variables += '\t'+'static int relleno_ventana = 0;' + '\n'
    if len(maximo) > 0:
        for i in range(len(maximo)):
            variables += '\t' + tipos[campos.index(maximo[i])] +' maxaux_' + maximo[i] + ' = -9999999;' + '\n'
    if len(minimo) > 0:
        for i in range(len(minimo)):
            variables += '\t' + tipos[campos.index(minimo[i])] + ' minaux_' + minimo[i] + ' = 9999999;' + '\n'
    if len(media) > 0:
        for i in range(len(media)):
            variables += '\t' + tipos[campos.index(media[i])] + ' avgaux_' + media[i] + ' = 0;' + '\n'
    if len(suma) > 0:
        for i in range(len(suma)):
            variables += '\t' + tipos[campos.index(suma[i])] + ' sumaux_' + suma[i] + ' = 0;' + '\n'
    return variables

#Crear las condiciones que tiene que cumplir para generar la respuesta.
def consumir_eventos(expression, campos, tipos, ontologia, ffunciones, fcabecera): #funciones, 
    #print('Setencia SQL: ' + expression)
    lexer = sqlLexer(InputStream(expression))
    stream = CommonTokenStream(lexer)
    parser = sqlParser(stream)
    tree = parser.start()
    printer = sqlListenerEval(campos, tipos, ontologia)
    walker = ParseTreeWalker()
    walker.walk(printer, tree)
    condiciones = printer.condiciones
    condiciones2 = printer.condiciones2
    maximo = printer.maximo
    minimo = printer.minimo
    media = printer.media
    suma = printer.suma
    todo = printer.todo
    ventana =  printer.window
    where = printer.where
    camposselect = printer.camposselect
    valido = printer.correcto

    if valido == True:
        if where == 1:
            answer = sqlVisitorEval(condiciones, ffunciones, fcabecera, ontologia).visit(tree)    
            ffunciones.write(answer)
            ffunciones.write('\t' + 'if (condicion' + str(condiciones - 1) + ' == 1 ){' + '\n')
            ffunciones.write('\t\t' + 'return 0;' + '\n')
            ffunciones.write('\t' + '}else{' + '\n')
            ffunciones.write('\t\t' + 'return 1;' + '\n')
            ffunciones.write('\t' + '}' + '\n' )
            ffunciones.write('}' + '\n')
        else:
            ffunciones.write('int ValidarEvento(' + ontologia +' *a){' + '\n')
            ffunciones.write('\t' + 'return 0' + '\n')
            ffunciones.write('}' + '\n')
        nombre = define_consumir_eventos(camposselect, maximo, minimo, media, suma, todo, tipos, campos, ontologia)
        ffunciones.write(nombre + '{' + '\n')
        fcabecera.write(nombre + ';' + '\n')
        if todo == 0:
            ffunciones.write(generar_variables_funciones(maximo, minimo, media, suma, tipos, campos, ventana, ontologia))
        ffunciones.write('\t' + 'if (ValidarEvento(a) == 0) {' + '\n')
        if todo == 0:
            ffunciones.write(generar_respuesta(maximo, minimo, media, suma, camposselect, todo, ventana, campos))
        ffunciones.write('\t\t' + 'return 0;' + '\n')
        ffunciones.write('\t' + '}else{' + '\n')
        ffunciones.write('\t\t' + 'return 1;' + '\n')
        ffunciones.write('\t' + '}' + '\n')
        ffunciones.write('}' + '\n')
    return valido

'''******************* METODO MAIN PARA LA CREACION DEL FICHERO *******************'''
#Leer archivo json y almacenarlo en una variable
#print ("Leer archivos")
arg1 = str(sys.argv[1])
arg2 = str(sys.argv[2])
def_ontologia = json.loads(open(arg1).read()) # Leemos la definicion de la ontología.'JsonLectura_Termostato.json'
#print (def_ontologia)

campos = []
tipo_campos = []
sql = open(arg2).read()#"select temp from Lectura_Termostato where (temp>15.0)and(hum=5)" #tengo que hacer algo para pasar esto.
ontologia = def_ontologia['required']

#Extraer los campos definidos en la ontologia.
for key, value in def_ontologia['datos']['properties'].items():
#    print (key)
#    print (value)
    campos.append(key)
    tipo_campos.append(tipo_dato(value['type']))
print (campos)
print (tipo_campos)

#Creacion de los ficheros
ficfunciones = open("../Ficheros_Generados/Ontologia/ontologia.c", "w") #Fichero con las funciones para el tratamiento de la informacion
ficcabecera = open("../Ficheros_Generados/Ontologia/ontologia.h" , "w")  #Fichero con las cabeceras de las funciones que se van a crear

#Definicion del fichero de cabecera
ficcabecera.write("#ifndef _ONTOLOGIA_H" + "\n")
ficcabecera.write("#define _ONTOLOGIA_H" + "\n\n")
ficcabecera.write("#define MAX_KP_INSTANCE_NAME 32" + "\n\n")

#Creacion del contenido de los ficheros
crea_estructura(ficcabecera, tipo_campos, campos, ontologia[0]) #Creación de la estructura para el almacenamiento de los eventos
ficcabecera.write('\n')
#escribe_cabeceras(ficfunciones) #Librerias necesarias para el tratamiento de la informacion
#ficfunciones.write('\n')
eventos_recibidos(ficfunciones, ficcabecera, campos, tipo_campos, ontologia[0])
consumir_eventos(sql, campos, tipo_campos, ontologia[0], ficfunciones, ficcabecera)
#print('Terminamos el consumir eventos' + '\n')
#generar_fichero(ficfunciones, ficcabecera, campos, tipo_campos, sql)
ficcabecera.write("#endif")

#Cierre de los fichero
ficcabecera.close()
ficfunciones.close()




