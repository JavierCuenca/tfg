#!/usr/bin/python3
from antlr4 import *
from sqlLexer import sqlLexer
from sqlVisitor import sqlVisitor
from sqlParser import sqlParser
import sys
from pprint import pprint
import codecs
import sys

# This class defines a complete generic visitor for a parse tree produced by sqlParser.

class sqlVisitorEval(sqlVisitor):

    # Visit a parse tree produced by sqlparte1Parser#expr.
    cond = 0
    condicion = ''

    def __init__(self, condiciones, ffunciones, fcabecera, ontologia):
        self.condiciones = condiciones
        self.ffcunciones = ffunciones
        self.fcabecera = fcabecera
        #ffunciones.write()
        #print('condiciones a crear ' + str(condiciones))
        ffunciones.write('int ValidarEvento(' + ontologia + '_t a){' + '\n')
        fcabecera.write('int ValidarEvento(' + ontologia + '_t a);' + '\n')
        for i in range(condiciones):
            #print('valor de i ' + str(i) + ', valor de condiciones ' + str(condiciones))
            if i == 0 :
                #print('priemera condicion creada')
                ffunciones.write('\t' + 'int condicion' + str(i))
            else:
                #print('condicion ' + str(condiciones))
                ffunciones.write(', condicion' + str(i))
        #print('condicion final' + str(condiciones))
        ffunciones.write(';' + '\n')
        #ffunciones.write('\t' + 'for (i=0; i < num_datos; i++){ ' + '\n')

    # Visit a parse tree produced by sqlParser#start.
    def visitStart(self, ctx:sqlParser.StartContext):
        #print("visitStart",ctx.getText())
        '''self.condicion += '\t' + 'if (condicion' + str(self.cond - 1) + ' == 1 ){' + '\n'
        self.condicion += '\t\t' + 'return 0;' + '\n'
        self.condicion += '\t' + '}else{;' + '\n'
        self.condicion += '\t\t' + 'return 1;' + '\n'
        self.condicion += '\t' + '}' + '\n'
        ffunciones.write('\t' + 'if (condicion' + str(condiciones - 1) + ' == 1 ){' + '\n')
        ffunciones.write('\t\t' + 'return 0;' + '\n')
        ffunciones.write('\t' + '}else{' + '\n')
        ffunciones.write('\t\t' + 'return 1;' + '\n')
        ffunciones.write('\t' + '}' + '\n')'''
        return self.visit(ctx.sql())


    # Visit a parse tree produced by sqlParser#SentenciaSQL.
    '''def visitSentenciaSQL(self, ctx:sqlParser.SentenciaSQLContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#SentenciaSelectAgre.
    def visitSentenciaSelectAgre(self, ctx:sqlParser.SentenciaSelectAgreContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#SentenciaSelectSimple.
    def visitSentenciaSelectSimple(self, ctx:sqlParser.SentenciaSelectSimpleContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#ClausulaAgreSelect.
    def visitClausulaAgreSelect(self, ctx:sqlParser.ClausulaAgreSelectContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#ClausulasimpleSelect.
    def visitClausulasimpleSelect(self, ctx:sqlParser.ClausulasimpleSelectContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#ClausulasimpleSelectAll.
    def visitClausulasimpleSelectAll(self, ctx:sqlParser.ClausulasimpleSelectAllContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#FuncionesMax.
    def visitFuncionesMax(self, ctx:sqlParser.FuncionesMaxContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#FuncionesMin.
    def visitFuncionesMin(self, ctx:sqlParser.FuncionesMinContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#FuncionesSum.
    def visitFuncionesSum(self, ctx:sqlParser.FuncionesSumContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#FuncionesAvg.
    def visitFuncionesAvg(self, ctx:sqlParser.FuncionesAvgContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#FuncionWindow.
    def visitFuncionWindow(self, ctx:sqlParser.FuncionWindowContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#ExprSelect.
    def visitExprSelect(self, ctx:sqlParser.ExprSelectContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#SentenciaFrom.
    def visitSentenciaFrom(self, ctx:sqlParser.SentenciaFromContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by sqlParser#SentenciaWhere.
    def visitSentenciaWhere(self, ctx:sqlParser.SentenciaWhereContext):
        return self.visitChildren(ctx)'''
    
    # Visit a parse tree produced by sqlParser#opSentenceWhere.
    def visitOpSentenceWhere(self, ctx:sqlParser.OpSentenceWhereContext):
        #print ('He llegado a la expresion')
        left = self.visit(ctx.left)
        rigth = self.visit(ctx.rigth)
        op = ctx.op.text

        opchar = op
        if opchar == 'and' :
            self.condicion += '\t' + 'if ((condicion' + str(self.cond - 2) +' == 1) && ( condicion' + str(self.cond - 1) + ' == 1)) { ' + '\n' + '\t\t\t condicion' + str(self.cond) + ' = 1;' + '\n' + '\t }else{' + '\n' + '\t\t condicion' + str(self.cond) + ' = 0;' + '\n' + '\t }' +'\n'
        elif opchar == 'or' :
            self.condicion += '\t' + 'if ((condicion' + str(self.cond - 2) +' == 1) || ( condicion' + str(self.cond - 1) + ' == 1)) { ' + '\n' + '\t\t condicion' + str(self.cond) + ' = 1;' + '\n' + '\t }else{' + '\n' + '\t\t condicion' + str(self.cond) + ' = 0;' + '\n' + '\t }' +'\n'
        else:
            raise ValueError("Unknown operator " + op)
        return self.condicion
        
    # Visit a parse tree produced by sqlParser#campoExprWhere.
    def visitCampoExprWhere(self, ctx:sqlParser.CampoExprWhereContext):
        #print("visitCampo",ctx.getText())
        return ctx.getText()


    # Visit a parse tree produced by sqlParser#parenExprWhere.
    def visitParenExprWhere(self, ctx:sqlParser.ParenExprWhereContext):
        #print("visitParen",ctx.getText())
        return self.visit(ctx.exprwhere())


    # Visit a parse tree produced by sqlParser#opExprWhere.
    def visitOpExprWhere(self, ctx:sqlParser.OpExprWhereContext):
        #print ('He llegado a la expresion')
        left = self.visit(ctx.left)
        rigth = self.visit(ctx.rigth)
        op = ctx.op.text

        condicion = ''  
        opchar = op
        if opchar == '<=' :
            self.condicion += '\t' + 'condicion' + str(self.cond) +' = ( a.' + left + ' ' + opchar + ' ' + rigth + ') ? 1 : 0;' + '\n'
        elif opchar == '>=' :
            self.condicion += '\t' + 'condicion' + str(self.cond) +' = ( a.' + left + ' ' + opchar + ' ' + rigth + ') ? 1 : 0;' + '\n'
        elif opchar == '=' :
            self.condicion += '\t' + 'condicion' + str(self.cond) +' = ( a.' + left + ' == '  + rigth + ') ? 1 : 0;' + '\n'
        elif opchar == '<>' :
            self.condicion += '\t' + 'condicion' + str(self.cond) +' = ( a.' + left + ' ' + opchar + ' ' + rigth + ') ? 1 : 0;' + '\n'
        elif opchar == '<' :
            self.condicion += '\t' + 'condicion' + str(self.cond) +' = ( a.' + left + ' ' + opchar + ' ' + rigth + ') ? 1 : 0;' + '\n'
        elif opchar == '>' :
            self.condicion += '\t' + 'condicion' + str(self.cond) +' = ( a.' + left + ' ' + opchar + ' ' + rigth + ') ? 1 : 0;' + '\n'
        else :
            raise ValueError("Unknown operator " + op) 
        self.cond = self.cond + 1
        return self.condicion

