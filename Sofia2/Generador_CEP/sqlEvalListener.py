#!/usr/bin/python3
from antlr4 import *
from sqlLexer import sqlLexer
from sqlParser import sqlParser
from sqlListener import sqlListener
import sys
from pprint import pprint
import codecs
import sys

# This class defines a complete listener for a parse tree produced by sqlParser.
class sqlListenerEval(sqlListener):

    condiciones = 0
    condiciones2 = 0
    where = 0
    maximo = []
    minimo = []
    media = []
    suma = []
    camposselect = []
    todo = 0
    window = 0
    camposeleccionado = ''
    correcto = True

    def __init__(self, campos, tipos, ontologia):
        self.campos = campos
        self.tipos = tipos
        self.ontologia = ontologia

    # Exit a parse tree produced by sqlParser#start.
   # def exitStart(self, ctx:sqlParser.StartContext):
        #print (self.condiciones)
        #print (self.condiciones2)

    # Exit a parse tree produced by sqlParser#ClausulaAgreSelect.
    def exitClausulaAgreSelect(self, ctx:sqlParser.ClausulaAgreSelectContext):
        operacion = ctx.getText()
        operador = operacion[:operacion.find('(')]
        if operador != 'MAX' and operador != 'MIN' and operador != 'AVG' and operador != 'SUM' and operador != 'max' and operador != 'min' and operador != 'avg' and operador != 'sum':
            self.correcto = False
        


    # Exit a parse tree produced by sqlParser#ClausulasimpleSelect.
    def exitClausulasimpleSelect(self, ctx:sqlParser.ClausulasimpleSelectContext):
        if self.correcto == True:
            #print('campo ' + ctx.getText())
            self.camposselect = ctx.getText().split(',')

    # Exit a parse tree produced by sqlParser#ClausulasimpleSelectAll.
    def exitClausulasimpleSelectAll(self, ctx:sqlParser.ClausulasimpleSelectAllContext):
        self.todo = 1

    # Exit a parse tree produced by sqlParser#FuncionesMax.
    def exitFuncionesMax(self, ctx:sqlParser.FuncionesMaxContext):
        if self.correcto == True:
            self.maximo.append(self.camposeleccionado)

    # Exit a parse tree produced by sqlParser#FuncionesMin.
    def exitFuncionesMin(self, ctx:sqlParser.FuncionesMinContext):
        if self.correcto == True:
            self.minimo.append(self.camposeleccionado)

    # Exit a parse tree produced by sqlParser#FuncionesSum.
    def exitFuncionesSum(self, ctx:sqlParser.FuncionesSumContext):
        if self.correcto == True:
            self.suma.append(self.camposeleccionado)

    # Exit a parse tree produced by sqlParser#FuncionesAvg.
    def exitFuncionesAvg(self, ctx:sqlParser.FuncionesAvgContext):
        if self.correcto == True:
            self.media.append(self.camposeleccionado)

    # Exit a parse tree produced by sqlParser#FuncionWindow.
    def exitFuncionWindow(self, ctx:sqlParser.FuncionWindowContext):
        if self.correcto == True:
            numero = ctx.getText()[6:]
            #print('Parametro window recogido: ' + numero)
            try:
                self.window = int(numero)
                #print('es un numero')
            except:
                #print('no es un numero')
                self.correcto = False

    # Enter a parse tree produced by sqlParser#ExprSelect.
    def enterExprSelect(self, ctx:sqlParser.ExprSelectContext):
        campo = ctx.getText()
        #print('campo elegido ' + campo)
        if not campo in self.campos :
            self.correcto = False
        else:
            self.camposeleccionado = campo

    # Enter a parse tree produced by sqlParser#SentenciaFrom.
    def enterSentenciaFrom(self, ctx:sqlParser.SentenciaFromContext):
        if self.correcto == True:
            data = ctx.getText()
            #print('sentencia from ' + data)
            ont = data[4:]
            #print('ontologia ' + ont)
            if ont != self.ontologia :
                self.correcto = False

    def exitSentenciaWhere(self, ctx:sqlParser.SentenciaWhereContext):
        if self.correcto == True:
            self.where = 1

    # Enter a parse tree produced by sqlParser#opSentenceWhere.
    def enterOpSentenceWhere(self, ctx:sqlParser.OpSentenceWhereContext):
        if self.correcto == True:
            self.condiciones += 1
            self.condiciones2 += 1

    # Enter a parse tree produced by sqlParser#opExprWhere.
    def enterOpExprWhere(self, ctx:sqlParser.OpExprWhereContext):
        if self.correcto == True:
            self.condiciones += 1
            #campos = ['temp', 'hum'] # Pasar los que creo cuando empiece a hacer lo otro
            #datos = ['double', 'int'] # Pasar los que creo cuando empiece a hacer lo otro
            left = ctx.left.getText()
            rigth = ctx.rigth.getText()
            #print('Tipo de dato = ' + str(type(rigth)))
            if self.correcto == True:
                #print('tipo de datos: ' + str(self.tipos[self.campos.index(left)]))
                if left in self.campos:
                    if self.tipos[self.campos.index(left)] == 'int':
                        try:
                            #print('comprobacion de int ' + rigth)
                            int(rigth)
                            self.condiciones2 += 1
                        except:
                            self.correcto = False

                        '''print (str(int(rigth)))
                        if int(rigth) > 0:
                            self.condiciones2 += 1
                        else:
                            self.correcto = False'''
                    elif self.tipos[self.campos.index(left)] == 'float':
                        #print('comprobacion de float ' + rigth)
                        try:
                            float(rigth)
                            self.condiciones2 += 1
                        except:
                            self.correcto = False

                        '''print (str(float(rigth)))
                        if float(rigth) > 0.0:
                            self.condiciones2 += 1
                        else:
                            self.correcto = False'''
                    elif self.tipos[campos.index(left)] == 'char *':
                        #print('comprobacion de char' + rigth)
                        try:
                            str(rigth)
                            self.condiciones2 += 1
                        except:
                            self.correcto = False
                else:
                    self.correcto = False

        

    # Exit a parse tree produced by sqlparte2Parser#opExpr.
    #def exitOpExpr(self, ctx:sqlparte2Parser.OpExprContext):
    #   self.condiciones += 1

    # Exit a parse tree produced by sqlParser#opExprWhere.


def main():
    #lexer = arithmeticLexer(StdinStream())
    tipos = ['float', 'int']
    campos = ['temp', 'hum']
    ontologia = 'Lectura_Termostato'
    expression = 'select max(temp), min(hum), window 10 from Lectura_Termostato where (temp>15.0)and(hum=5)'
    lexer = sqlLexer(InputStream(expression))
    stream = CommonTokenStream(lexer)
    parser = sqlParser(stream)
    tree = parser.start()
    printer = sqlListenerEval(campos, tipos, ontologia)
    walker = ParseTreeWalker()
    walker.walk(printer, tree)
    condiciones = printer.condiciones
    condiciones2 = printer.condiciones2
    maximo = printer.maximo
    minimo = printer.minimo
    media = printer.media
    suma = printer.suma
    todo = printer.todo
    ventana =  printer.window
    where = printer.where
    camposselect = printer.camposselect
    valido = printer.correcto
    #print('Estamos fuera y el numero de condiciones es: ' + str(condiciones))
    #print('Estamos fuera y el numero de condiciones buenas es: ' + str(condiciones2))
    #print('campos maximo: ' + str(maximo))
    #print('campos minimo: ' + str(minimo))
    #print('campos media: ' + str(media))
    #print('campos suma: ' + str(suma))
    #print('campos todo: ' + str(todo))
    #print('numero de elementos a recuparar: ' + str(ventana) )
    #print('campos camposselect: ' + str(camposselect))
    #print('Es valido ' + str(valido))
    #print('Hay where: ' + str(where))

if __name__ == '__main__':
    main()


