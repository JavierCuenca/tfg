#include "tokenizer.h"

unsigned int idx = 0;

const char field_names[8][MAX_TOKEN_LEN+1]  = {"_id\0","contextData\0", "timestamp\0",
		"kpInstance\0","$date\0","Lectura_Termostato\0","temp\0","hum\0"};


//char skip_spaces(char ptr[2048], unsigned int idx, unsigned char *npos) {
char skip_spaces(char ptr[2048]) {
	char c;
	unsigned char cnt = 0;
	do {
//		c = ptr[idx+cnt];
//		cnt++;
		c = ptr[idx++];
	} while ( (c == ' ') || (c == '\r') || (c == '\n') || (c == '\t'));

//	*npos = cnt;
	return c;
}

//token_t next_token(char ptr[2048],char token[MAX_TOKEN_LEN+1], unsigned int idx, unsigned char *npos) {
token_t next_token(char ptr[2048],char token[MAX_TOKEN_LEN+1]) {
#pragma HLS INLINE off

	static char c = ' ';
	static token_t ret = TK_ERROR;

	unsigned char i;
	//, cnt = 0;

	if (ret != TK_NUM) {
//		c = skip_spaces(ptr,idx,&cnt);
		c = skip_spaces(ptr);

	} else if  ((c == ' ') || (c == '\r') || (c == '\n') || (c == '\t')) {
		//En este caso, no consumimos nada si el último carácter leído fue un espacio
//		c = skip_spaces(ptr,idx, &cnt);
		c = skip_spaces(ptr);
	}

	switch (c) {
	case '[':
	case ']':
		ret = TK_ABRE_CORCHETE;
		break;
	case FIN_CADENA:
		ret = TK_EOF;
		break;
	case '{':
		ret = TK_ABRE_LLAVE;
		break;
	case '}':
		ret = TK_CIERRA_LLAVE;
		break;
	case ':':
		ret = TK_DOS_PUNTOS;
		break;
	case ',':
		ret = TK_COMA;
		break;
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		i = 0;
	//	if (token)
			token[i++] = c;
	//	else
	//		i++;

		do {
//			c = ptr[idx+cnt];
//			cnt++;
			c = ptr[idx++];
	//		if (token)
				token[i++] = c;
	//		else
	//			i++;
		} while ( (i < MAX_TOKEN_LEN) && (c != FIN_CADENA) && ( ((c >= 48) && (c <=57)) || c == '.') );
	//	if (token)
			token[i-1] = 0;
		if (c == FIN_CADENA) {
			ret = TK_EOF;
		} else if ( i == MAX_TOKEN_LEN ) {
			ret = TK_MAX_LEN;
		} else {
			ret = TK_NUM;
		}
		break;
	case '"':
		i = 0;
		do {
//			c = ptr[idx+cnt];
//			cnt++;
			c = ptr[idx++];
	//		if (token)
				token[i++] = c;
	//		else
	//			i++;
		} while ( (i < MAX_TOKEN_LEN) && (c != '"') && (c != FIN_CADENA));
//		if (token)
			token[i-1] = 0;

		if (c == FIN_CADENA) {
			ret = TK_EOF;
		} else if ( (i == MAX_TOKEN_LEN) && (c != '"')) {
			ret = TK_MAX_LEN;
		} else {
			ret = TK_ID;
		}
		break;
	default:
		ret = TK_ERROR;
	}
//	*npos = cnt;

	return ret;
}

int check_str(char str1[MAX_TOKEN_LEN+1],const char str2[MAX_TOKEN_LEN+1], volatile int len) {
#pragma HLS INLINE off
int i = 0,j = 1;
//	for (int i = 0; i < _MIN(MAX_TOKEN_LEN+1,len) ; i++) {
//	for (i = 0; i < len ; i++) {
//		if ( str1[i] != str2[i]) return 0;
//	}

	do {
		if (str1[i] != str2[i]) j = 0;
	} while (str2[++i] != '\0');
//	if (str1[0] != str2[0]) return 0;
//	if (str1[len-1] != str2[len-1]) return 0;

	return j;
}

unsigned char get_value_date(char str1[MAX_TOKEN_LEN+1], char str2[MAX_TOKEN_LEN+1], unsigned char idx){
	static char c = ' ';
	unsigned char i = 0;
	unsigned char cnt = 0;
	do{
		c = str1[idx + cnt];
		cnt++;
		str2[i] = c;
		i++;
	}while((i < MAX_TOKEN_LEN) && (c != '-') && (c != 'T') && (c != ':') && (c != '.') && (c != 'Z') && (c != FIN_CADENA));
	if (str2) str2[i-1] = 0;
	return cnt;
}
