  state_t st = ST_ESPERA;
  static state_t next_st = ST_ESPERA;
  unsigned char i;
  unsigned char npos_date;

  int ret = 0;

  token_t tk1,tk2,tk3;
  char id[MAX_TOKEN_LEN+1],attr[MAX_TOKEN_LEN+1],val[MAX_TOKEN_LEN+1], trash1[MAX_TOKEN_LEN+1],trash2[MAX_TOKEN_LEN+1];
  char date[MAX_TOKEN_LEN+1];
  int len = 0;

  while ((next_st != ST_ERROR) && (next_st != ST_EVENTO)) {
    st = next_st;


    switch (st) {
    case ST_ESPERA:
      tk1 = next_token(ptr,trash1);
      if (tk1 == TK_ABRE_LLAVE) {
        next_st = ST_ID;
      }
      break;
    case ST_ID:
      CONSUMIR_OBJ(field_names[0],3, ST_ID_ATTR_OID);
      break;
    case ST_ID_ATTR_OID:

      next_st = ST_ERROR;

      tk1 = next_token(ptr,attr);
      tk2 = next_token(ptr,trash1);
      tk3 = next_token(ptr,val);

      if (( tk1 == TK_ID) && (tk2 == TK_DOS_PUNTOS) && (tk3 == TK_ID)) next_st = ST_FIN_ID;
      break;
    case ST_FIN_ID:
      next_st = ST_ERROR;

      tk1 = next_token(ptr,trash1);

      tk2 = next_token(ptr,trash2);

      if (( tk1 == TK_CIERRA_LLAVE) && (tk2 == TK_COMA)) next_st = ST_CONTEXT_DATA;
      break;

    case ST_CONTEXT_DATA:
      CONSUMIR_OBJ(field_names[1],11,ST_CONTEXT_DATA_ATRIBUTOS);
      break;
    case ST_CONTEXT_DATA_ATRIBUTOS:
      next_st = ST_ERROR;
      tk1 = next_token(ptr,attr);

      if (tk1 == TK_ID) {
        tk2 = next_token(ptr,trash1);

        if (check_str(attr,field_names[2],len = 9) && (tk2 == TK_DOS_PUNTOS)) {
          next_token(ptr,trash2);

          next_st = ST_TIME_STAMP;
        }else if (check_str(attr, field_names[3],len = 10) && (tk2 == TK_DOS_PUNTOS)){
          next_st = ST_KP_INSTANCE;
        }else if ( tk2 == TK_DOS_PUNTOS ) {
          next_st = ST_CONTEXT_ATRIBUTO;
        }
      }
      break;
    case ST_CONTEXT_ATRIBUTO:
      next_st = ST_ERROR;
      tk1 = next_token(ptr,trash1);
      tk2 = next_token(ptr,trash2);

      if ((tk1 == TK_ID) && (tk2 == TK_COMA)) {
        next_st = ST_CONTEXT_DATA_ATRIBUTOS;
      }
      else if ((tk1 == TK_ID) && (tk2 == TK_CIERRA_LLAVE)) {
        next_st = ST_FIN_CONTEXT_DATA;
      }
      break;

    case ST_KP_INSTANCE:
      next_st = ST_ERROR;
      tk3=next_token(ptr,val);

      i = 0;
      do {
        instancia->kpinstance[i] = val[i];
        i++;
      }while ((i < MAX_TOKEN_LEN) && (val[i] != FIN_CADENA));
      next_st = ST_FIN_KP_INSTANCE;
      break;

    case ST_TIME_STAMP:
      next_st = ST_ERROR;
      tk1 = next_token(ptr,attr);

      tk2 = next_token(ptr,trash1);

      tk3 = next_token(ptr,val);


      if (( tk1 == TK_ID) && check_str(attr,field_names[4], len = 5) && (tk2 == TK_DOS_PUNTOS) && (tk3 == TK_ID)) {

        npos_date = 0;
        npos_date += get_value_date(val, date, npos_date);
        instancia->year = HlsAtoi(date);
        npos_date += get_value_date(val, date, npos_date);
        instancia->month = HlsAtoi(date);
        npos_date += get_value_date(val, date, npos_date);
        instancia->day = HlsAtoi(date);
        npos_date += get_value_date(val, date, npos_date);
        instancia->hour = HlsAtoi(date);
        npos_date += get_value_date(val, date, npos_date);
        instancia->minutes = HlsAtoi(date);
        npos_date += get_value_date(val, date, npos_date);
        instancia->seconds = HlsAtoi(date);
        npos_date += get_value_date(val, date, npos_date);
        instancia->miliseconds = HlsAtoi(date);

        next_st = ST_FIN_TIME_STAMP;
      }
      //Else --> error
      break;
    case ST_FIN_KP_INSTANCE:
      next_st = ST_ERROR;

      tk1 = next_token(ptr,trash1);

      if (tk1 == TK_COMA) next_st = ST_CONTEXT_DATA_ATRIBUTOS;
      //Else error
      break;
    case ST_FIN_TIME_STAMP:
      next_st = ST_ERROR;

      tk1 = next_token(ptr,trash1);

      if ( tk1 == TK_CIERRA_LLAVE ) next_st = ST_FIN_CONTEXT_DATA;
      else if (tk1 == TK_COMA) next_st = ST_CONTEXT_DATA_ATRIBUTOS;
      //Else error
      break;
    case ST_FIN_CONTEXT_DATA:
      next_st = ST_ERROR;
      tk1 = next_token(ptr,trash1);

      tk2 = next_token(ptr,trash2);

      if ((tk2 == TK_COMA) && (tk1 == TK_CIERRA_LLAVE)) next_st = ST_ONTOLOGIA;

      break;