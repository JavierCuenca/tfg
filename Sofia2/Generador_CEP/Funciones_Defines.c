#define CONSUMIR_OBJ(IDENTIFICADOR, LEN, ESTADO_EXITO) {  \
    int check_res = 0; \
    next_st = ST_ERROR;           \
    tk1 = next_token(ptr,id); \
    tk2 = next_token(ptr,trash1);         \
    tk3 = next_token(ptr,trash2);         \
    check_res = check_str(id,IDENTIFICADOR,len = LEN); \
    if (( tk1 == TK_ID) && (check_res == 1) && (tk2 == TK_DOS_PUNTOS) && (tk3 == TK_ABRE_LLAVE)) \
    next_st = ESTADO_EXITO;           \
}

#define CONSUMIR_ATRB_ONTOLOGIA_FLOAT(ATRIBUTO) { \
    next_st = ST_ERROR;             \
    tk3 = next_token(ptr,val);          \
    if ( tk3 == TK_NUM) {           \
      ATRIBUTO = HlsAtof(val);            \
      next_st = ST_ONTOLOGIA_ATRIBUTOS;         \
    }                 \
}

#define CONSUMIR_ATRB_ONTOLOGIA_INT(ATRIBUTO) { \
    next_st = ST_ERROR;             \
    tk3 = next_token(ptr,val);          \
    if ( tk3 == TK_NUM ) {            \
      ATRIBUTO = HlsAtoi(val);            \
      next_st = ST_ONTOLOGIA_ATRIBUTOS;         \
    }                 \
}



float HlsAtof(char str[MAX_TOKEN_LEN+1])
{

  int res = 0;
  unsigned char i = (str[0]=='-')?1:0;
  unsigned char decimal = 0;
  unsigned char npos = 0;
  float num = 0.0;

  for (; str[i] != '\0' && i <= MAX_TOKEN_LEN; ++i) {
    if (decimal == 1)
      npos++;

    if (str[i] != '.')
      res = res * 10 + str[i] - '0';
    else
      decimal = 1;
  }
  if (str[0] == '-')
    res = -res;
  num = (res/(10*npos));
#ifndef __SYNTHESIS__
  printf(">>>>MY ATOF: %s is %f\n",str,num);
#endif
  return num;

}

int HlsAtoi(char str[MAX_TOKEN_LEN+1])
{
  int res = 0;
  int i = (str[0]=='-')?1:0;

  for (; str[i] != '\0' && i <= MAX_TOKEN_LEN; ++i)
    res = res * 10 + str[i] - '0';
  if (str[0] == '-')
    res = -res;
#ifndef __SYNTHESIS__
  printf(">>>>MY ATOI: %s is %d\n",str,res);
#endif
  return res;
}


//Devuelve 0 en caso de éxito. 1 en caso de error.

