#ifndef _ONTOLOGIA_H
#define _ONTOLOGIA_H

#define MAX_KP_INSTANCE_NAME 32

typedef struct {
	float temp;
	int hum;
	unsigned short year;
	unsigned char month;
	unsigned char day;
	unsigned char hour;
	unsigned char minutes;
	unsigned char seconds;
	unsigned short miliseconds;
	char kpinstance[MAX_KP_INSTANCE_NAME];
} Lectura_Termostato_t;

typedef enum {ST_ESPERA,ST_ID,ST_ID_ATTR_OID,ST_FIN_ID,ST_CONTEXT_DATA,ST_CONTEXT_DATA_ATRIBUTOS,ST_CONTEXT_ATRIBUTO,ST_FIN_CONTEXT_DATA,ST_KP_INSTANCE, ST_FIN_KP_INSTANCE,ST_TIME_STAMP,ST_FIN_TIME_STAMP,ST_ONTOLOGIA,ST_ONTOLOGIA_ATRIBUTOS,ST_ONTOLOGIA_ATTR_TEMP,ST_ONTOLOGIA_ATTR_HUM,ST_FIN_ONTOLOGIA,ST_FIN_EVENTO,ST_EVENTO,ST_ERROR} state_t;

int RecibirEvento(char ptr[],Lectura_Termostato_t *instancia);



#endif
