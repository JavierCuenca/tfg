#include <stdio.h>
#include <stdlib.h>


#include "../tokenizer.h"
#include "ontologia.h"

#define _DEBUG


#define CONSUMIR_OBJ(IDENTIFICADOR, LEN, ESTADO_EXITO) {	\
		int check_res = 0; \
		next_st = ST_ERROR;						\
		tk1 = next_token(ptr,id); \
		tk2 = next_token(ptr,trash1);					\
		tk3 = next_token(ptr,trash2);					\
		check_res = check_str(id,IDENTIFICADOR,len = LEN); \
		if (( tk1 == TK_ID) && (check_res == 1) && (tk2 == TK_DOS_PUNTOS) && (tk3 == TK_ABRE_LLAVE)) \
		next_st = ESTADO_EXITO;						\
}

#define CONSUMIR_ATRB_ONTOLOGIA_FLOAT(ATRIBUTO) { \
		next_st = ST_ERROR;				      \
		tk3 = next_token(ptr,val);		      \
		if ( tk3 == TK_NUM) {			      \
			ATRIBUTO = HlsAtof(val);			      \
			next_st = ST_ONTOLOGIA_ATRIBUTOS;		      \
		}						      \
}

#define CONSUMIR_ATRB_ONTOLOGIA_INT(ATRIBUTO) { \
		next_st = ST_ERROR;				      \
		tk3 = next_token(ptr,val);		      \
		if ( tk3 == TK_NUM ) {			      \
			ATRIBUTO = HlsAtoi(val);			      \
			next_st = ST_ONTOLOGIA_ATRIBUTOS;		      \
		}						      \
}



float HlsAtof(char str[MAX_TOKEN_LEN+1])
{

	int res = 0;
	unsigned char i = (str[0]=='-')?1:0;
	unsigned char decimal = 0;
	unsigned char npos = 0;
	float num = 0.0;

	for (; str[i] != '\0' && i <= MAX_TOKEN_LEN; ++i) {
		if (decimal == 1)
			npos++;

		if (str[i] != '.')
			res = res * 10 + str[i] - '0';
		else
			decimal = 1;
	}
	if (str[0] == '-')
		res = -res;
	num = (res/(10*npos));
#ifndef __SYNTHESIS__
	printf(">>>>MY ATOF: %s is %f\n",str,num);
#endif
	return num;

}

int HlsAtoi(char str[MAX_TOKEN_LEN+1])
{
	int res = 0;
	int i = (str[0]=='-')?1:0;

	for (; str[i] != '\0' && i <= MAX_TOKEN_LEN; ++i)
		res = res * 10 + str[i] - '0';
	if (str[0] == '-')
		res = -res;
#ifndef __SYNTHESIS__
	printf(">>>>MY ATOI: %s is %d\n",str,res);
#endif
	return res;
}


//Devuelve 0 en caso de éxito. 1 en caso de error.
char *ontologia = "LecturaTermostato";

int RecibirEvento(char ptr[2048],Lectura_Termostato_t *instancia){
	state_t st = ST_ESPERA;
	static state_t next_st = ST_ESPERA;
	unsigned char i;
	unsigned char npos_date;

	int ret = 0;

	token_t tk1,tk2,tk3;
	char id[MAX_TOKEN_LEN+1],attr[MAX_TOKEN_LEN+1],val[MAX_TOKEN_LEN+1], trash1[MAX_TOKEN_LEN+1],trash2[MAX_TOKEN_LEN+1];
	char date[MAX_TOKEN_LEN+1];
	int len = 0;

	while ((next_st != ST_ERROR) && (next_st != ST_EVENTO)) {
		st = next_st;


		switch (st) {
		case ST_ESPERA:
			tk1 = next_token(ptr,trash1);
			if (tk1 == TK_ABRE_LLAVE) {
				next_st = ST_ID;
			}
			break;
		case ST_ID:
			CONSUMIR_OBJ(field_names[0],3, ST_ID_ATTR_OID);
			break;
		case ST_ID_ATTR_OID:

			next_st = ST_ERROR;

			tk1 = next_token(ptr,attr);
			tk2 = next_token(ptr,trash1);
			tk3 = next_token(ptr,val);

			if (( tk1 == TK_ID) && (tk2 == TK_DOS_PUNTOS) && (tk3 == TK_ID)) next_st = ST_FIN_ID;
			break;
		case ST_FIN_ID:
			next_st = ST_ERROR;

			tk1 = next_token(ptr,trash1);

			tk2 = next_token(ptr,trash2);

			if (( tk1 == TK_CIERRA_LLAVE) && (tk2 == TK_COMA)) next_st = ST_CONTEXT_DATA;
			break;

		case ST_CONTEXT_DATA:
			CONSUMIR_OBJ(field_names[1],11,ST_CONTEXT_DATA_ATRIBUTOS);
			break;
		case ST_CONTEXT_DATA_ATRIBUTOS:
			next_st = ST_ERROR;
			tk1 = next_token(ptr,attr);

			if (tk1 == TK_ID) {
				tk2 = next_token(ptr,trash1);

				if (check_str(attr,field_names[2],len = 9) && (tk2 == TK_DOS_PUNTOS)) {
					next_token(ptr,trash2);

					next_st = ST_TIME_STAMP;
				}else if (check_str(attr, field_names[3],len = 10) && (tk2 == TK_DOS_PUNTOS)){
					next_st = ST_KP_INSTANCE;
				}else if ( tk2 == TK_DOS_PUNTOS ) {
					next_st = ST_CONTEXT_ATRIBUTO;
				}
			}
			break;
		case ST_CONTEXT_ATRIBUTO:
			next_st = ST_ERROR;
			tk1 = next_token(ptr,trash1);
			tk2 = next_token(ptr,trash2);

			if ((tk1 == TK_ID) && (tk2 == TK_COMA)) {
				next_st = ST_CONTEXT_DATA_ATRIBUTOS;
			}
			else if ((tk1 == TK_ID) && (tk2 == TK_CIERRA_LLAVE)) {
				next_st = ST_FIN_CONTEXT_DATA;
			}
			break;

		case ST_KP_INSTANCE:
			next_st = ST_ERROR;
			tk3=next_token(ptr,val);

			i = 0;
			do {
				instancia->kpinstance[i] = val[i];
				i++;
			}while ((i < MAX_TOKEN_LEN) && (val[i] != FIN_CADENA));
			next_st = ST_FIN_KP_INSTANCE;
			break;

		case ST_TIME_STAMP:
			next_st = ST_ERROR;
			tk1 = next_token(ptr,attr);

			tk2 = next_token(ptr,trash1);

			tk3 = next_token(ptr,val);


			if (( tk1 == TK_ID) && check_str(attr,field_names[4], len = 5) && (tk2 == TK_DOS_PUNTOS) && (tk3 == TK_ID)) {

				npos_date = 0;
				npos_date += get_value_date(val, date, npos_date);
				instancia->year = HlsAtoi(date);
				npos_date += get_value_date(val, date, npos_date);
				instancia->month = HlsAtoi(date);
				npos_date += get_value_date(val, date, npos_date);
				instancia->day = HlsAtoi(date);
				npos_date += get_value_date(val, date, npos_date);
				instancia->hour = HlsAtoi(date);
				npos_date += get_value_date(val, date, npos_date);
				instancia->minutes = HlsAtoi(date);
				npos_date += get_value_date(val, date, npos_date);
				instancia->seconds = HlsAtoi(date);
				npos_date += get_value_date(val, date, npos_date);
				instancia->miliseconds = HlsAtoi(date);

				next_st = ST_FIN_TIME_STAMP;
			}
			//Else --> error
			break;
		case ST_FIN_KP_INSTANCE:
			next_st = ST_ERROR;

			tk1 = next_token(ptr,trash1);

			if (tk1 == TK_COMA) next_st = ST_CONTEXT_DATA_ATRIBUTOS;
			//Else error
			break;
		case ST_FIN_TIME_STAMP:
			next_st = ST_ERROR;

			tk1 = next_token(ptr,trash1);

			if ( tk1 == TK_CIERRA_LLAVE ) next_st = ST_FIN_CONTEXT_DATA;
			else if (tk1 == TK_COMA) next_st = ST_CONTEXT_DATA_ATRIBUTOS;
			//Else error
			break;
		case ST_FIN_CONTEXT_DATA:
			next_st = ST_ERROR;
			tk1 = next_token(ptr,trash1);

			tk2 = next_token(ptr,trash2);

			if ((tk2 == TK_COMA) && (tk1 == TK_CIERRA_LLAVE)) next_st = ST_ONTOLOGIA;

			break;
		case ST_ONTOLOGIA:
			CONSUMIR_OBJ(field_names[5],18,ST_ONTOLOGIA_ATRIBUTOS);
			break;
		case ST_ONTOLOGIA_ATRIBUTOS:
			next_st = ST_ERROR;
			tk1 = next_token(ptr,attr);

			if ( tk1 == TK_ID) {
				tk2 = next_token(ptr,trash1);

				if ( check_str(attr,field_names[6],len = 4) && (tk2 == TK_DOS_PUNTOS)) {
					next_st = ST_ONTOLOGIA_ATTR_TEMP;
				} else if (check_str(attr,field_names[7],len = 3) && (tk2 == TK_DOS_PUNTOS)) {
					next_st = ST_ONTOLOGIA_ATTR_HUM;
				}
			} else if (tk1 == TK_COMA) {
				next_st = ST_ONTOLOGIA_ATRIBUTOS;
			} else if (tk1 == TK_CIERRA_LLAVE) {
				next_st = ST_FIN_EVENTO;
			}
			break;
		case ST_ONTOLOGIA_ATTR_TEMP:
			CONSUMIR_ATRB_ONTOLOGIA_FLOAT(instancia->temp);
			break;
		case ST_ONTOLOGIA_ATTR_HUM:
			CONSUMIR_ATRB_ONTOLOGIA_INT(instancia->hum);
			break;
		case ST_FIN_EVENTO:
			next_st = ST_ERROR;

			tk1 = next_token(ptr,trash1);

			if ( tk1 == TK_CIERRA_LLAVE ) {
				next_st = ST_EVENTO;
			}
			//Else error
			break;
		case ST_ERROR:
		case ST_EVENTO:
			next_st = ST_ESPERA;
			break;
		default:
			next_st = ST_ESPERA;
			break;
		}
	}


	return (next_st==ST_ERROR)?1:0;

}
int ValidarEvento(Lectura_Termostato_t *e){
	int condicion0, condicion1, condicion2;
	condicion0 = ( e->temp > 15.0) ? 1 : 0;
	condicion1 = ( e->hum == 5) ? 1 : 0;
	if ((condicion0 == 1) && ( condicion1 == 1)) { 
		condicion2 = 1;
	}else{
		condicion2 = 0;
	}
	if (condicion2 == 1 ){
		return 0;
	}else{;
	return 1;
	}
}
int ConsumirEvento(Lectura_Termostato_t *e, float *temp ){
	static Lectura_Termostato_t dat_calc[0];
	int tam_ventana = 0, i = 0;
	static int pos_ventana = 0;
	static int relleno_ventana = 0;
	if (ValidarEvento(e) == 0) {
		*temp = e->temp;
		return 0;
	}else{
		return 1;
	}
}

int CEP(char ptr[2048],float *temp){
	Lectura_Termostato_t lt;

	RecibirEvento(ptr,&lt);
	return ConsumirEvento(&lt,temp);
}
