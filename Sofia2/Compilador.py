#!/usr/bin/python3
import subprocess
import sys
import os

arg1 = sys.argv[1]
arg2 = sys.argv[2]

directorio = os.getcwd()
print(directorio)

nuevo_directorio = directorio + '/Gramatica'
process = os.chdir(nuevo_directorio)
print(os.getcwd())

command_line = 'antlr4 -Dlanguage=Python3 -o ../Ficheros_Generados/Gramatica -visitor -listener sql.g4 '
process = subprocess.call(command_line, shell = True, stderr = True)
print(str(process))

nuevo_directorio = directorio + '/Generador_CEP'
if process == 0:
	os.chdir(nuevo_directorio)
else:
	print('A ocurrido un error en la ejecucion')

if process == 0:
	command_line = 'python3 generador.py ' + arg1 + ' ' + arg2
	process = subprocess.call(command_line, shell = True)

os.chdir(directorio)
print(os.getcwd())


