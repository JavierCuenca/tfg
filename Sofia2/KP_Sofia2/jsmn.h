#ifndef __JSMN_H_
#define __JSMN_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * JSON type identifier. Basic types are:
 * 	o Object
 * 	o Array
 * 	o String
 * 	o Other primitive: number, boolean (true/false) or null
 */
typedef enum { JOIN, ///< Create session with the SIB 
    LEAVE, ///< End session with the SIB 
    INSERT, ///< Send data to be stored in the RTDB 
    UPDATE, ///< Update data stored in the RTDB
    DELETE, ///< Delete data stored in the RTDB,
    QUERY, ///< Retrieve stored data 
    SUBSCRIBE, ///< Enable updates on data changes 
    UNSUBSCRIBE, ///< Disable updates on data changes 
    INDICATION, ///< Data change indication
    BULK, ///< Bulk request,
    CONFIG ///< Configuration data request
    
} SSAPMessageTypes;

/**
 * SSAP message directions enum
 */
typedef enum { REQUEST, ///< Request send to the SIB
  RESPONSE, ///< Request processed at the SIB without errors
  ERROR ///< Request processed at the SIB with errors
} SSAPMessageDirection;

/**
 * SSAP persistence types enum
 */
typedef enum {MONGODB, ///< Data persistence in MongoDB
  INMEMORY ///< Data persistence in memory
} SSAPPersistenceType;

/**
 * SSAP query types enums.
 */
typedef enum { NATIVE, ///< MongoDB-like query
  SQLLIKE, ///< SQL-LIKE query
  SIB_DEFINED, ///< SIB defined query 
  BDH, ///< Historical database (i.e. Hadoop) query 
  CEP ///< CEP query
} SSAPQueryType;

/**
 * Structure to hold the data of an SSAP message.
 */
typedef struct{
        /**
	 * The unique identifier of the request.
	 */
	char* messageId;
	
	/**
	 * The unique identifier of the session.
	 */
	char* sessionKey;
	
	/**
	 * The identifier of the ontology that is referenced by the message.
	 */
	char* ontology;

	/**
	 * The direction of the message
	 */
	SSAPMessageDirection direction;
	
	/**
	 * The type of the message
	 */
	SSAPMessageTypes messageType;

	/**
	 * The persistence type of the message
	 */
	//SSAPPersistenceType persistenceType;
	
	/**
	 * The message body. It stores the data.
	 */
	char* body;
} ssap_message;
typedef enum {
	JSMN_PRIMITIVE = 0,
	JSMN_OBJECT = 1,
	JSMN_ARRAY = 2,
	JSMN_STRING = 3
} jsmntype_t;

typedef enum {
	/* Not enough tokens were provided */
	JSMN_ERROR_NOMEM = -1,
	/* Invalid character inside JSON string */
	JSMN_ERROR_INVAL = -2,
	/* The string is not a full JSON packet, more bytes expected */
	JSMN_ERROR_PART = -3
} jsmnerr_t;

/**
 * JSON token description.
 * @param		type	type (object, array, string etc.)
 * @param		start	start position in JSON data string
 * @param		end		end position in JSON data string
 */
typedef struct {
	jsmntype_t type;
	int start;
	int end;
	int size;
#ifdef JSMN_PARENT_LINKS
	int parent;
#endif
} jsmntok_t;

/**
 * JSON parser. Contains an array of token blocks available. Also stores
 * the string being parsed now and current position in that string
 */
typedef struct {
	unsigned int pos; /* offset in the JSON string */
	unsigned int toknext; /* next token to allocate */
	int toksuper; /* superior token node, e.g parent object or array */
} jsmn_parser;

/**
 * Create JSON parser over an array of tokens
 */
void jsmn_init(jsmn_parser *parser);

/**
 * Run JSON parser. It parses a JSON data string into and array of tokens, each describing
 * a single JSON object.
 */
jsmnerr_t jsmn_parse(jsmn_parser *parser, const char *js, size_t len,
		jsmntok_t *tokens, unsigned int num_tokens);

#ifdef __cplusplus
}
#endif

#endif /* __JSMN_H_ */
