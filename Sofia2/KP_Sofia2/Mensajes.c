/******* SI PUEDO METO AQUI EL ENVIO Y RECEPCIÓN DE LOS MENSAJES****/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include "jsmn.h"
#include "Mensajes.h"

char result[4096];
int r;

int comprobar_json(char *json){
	jsmn_parser p;
	jsmn_init(&p);
	jsmntok_t buffer[2048];
	r = jsmn_parse(&p, json, strlen(json), buffer, sizeof(buffer)/sizeof(buffer[0]));
	if (r < 0) {
		printf("Failed to parse JSON: %d\n", r);
		return 1;
	}
	printf("*******************PASAMOS EL PARSER***********************\n");
	 /*Assume the top-level element is an object*/
	if (r < 1 || buffer[0].type != JSMN_OBJECT) { 
		printf("Object expected\n");
		return 1;	
	}
	return 0;
}
char *join_message(char *json_message, char *instance, char *token, char *host, char *page){

	strcpy(json_message, "{\"join\": true, \"instanceKP\": ");
	strcat(json_message, instance);
	strcat(json_message, ", \"token\": ");
	strcat(json_message, token);
	strcat(json_message, "}");
	
	if (comprobar_json(json_message) == 0) {
		sprintf(result,
		 "POST %s HTTP/1.0\r\n"
		 "Host: %s\r\n"
		 "Content-type: application/json\r\n"
		 "Content-length: %d\r\n\r\n"
		 "%s", page, host, strlen(json_message), json_message);
		
		return result;
	} else{
		return "";
	}

}

char *leave_message(char *json_message, char *sessionkey, char *host, char *page){

	strcpy(json_message, "{\"leave\": true, \"sessionKey\": ");
	strcat(json_message, sessionkey);
	strcat(json_message, "}");

	if (comprobar_json(json_message) == 0) {
		sprintf(result,
		 "POST %s HTTP/1.0\r\n"
		 "Host: %s\r\n"
		 "Content-type: application/json\r\n"
		 "Content-length: %d\r\n\r\n"
		 "%s", page, host, strlen(json_message), json_message);
		return result;

	} else {
		return "";
	}
}

char *query_message(char *json_message, char* ontology, char *sessionkey, char *query, char *querytype, char *host, char *page){
	strcpy(json_message, "{\"sessionKey\"");
	strcat(json_message, sessionkey);
	strcat(json_message, ", \"ontology\": ");
	strcat(json_message, ontology);
	strcat(json_message, ", \"query\": ");
	strcat(json_message, query);
	strcat(json_message, ", \"queryType\": ");
	strcat(json_message, querytype);
	strcat(json_message, "}");

	printf("Mensaje: %s\n", json_message);
	
	if (comprobar_json(json_message) == 0) {
		sprintf(result,
		 "GET %s HTTP/1.0\r\n"
		 "Host: %s\r\n"
		 "Content-type: application/json;charset=UTF-8\r\n"
		 "Content-length: %d\r\n\r\n"
		 "%s", page, host, strlen(json_message), json_message);
		printf("%s\n", result);
		return result;
	}else{
		return "";
	}
}

void get_sessionkey(char *message, char *sessionkey){
	char response[2048];
	char *res;
	strcpy(response, strstr(message, "\"sessionKey\""));
	res = strtok(response, ",");
	res = strstr(res, ":");
	strcpy(sessionkey, strstr(res, "\""));
	strcat(sessionkey, "\0");
}


/*char *get_mensaje(){ // Podria usar este para la obtencion de la respuesta y su proceso, ya vere que hago

	return result;
}*/


/*char *subscription_message(){

	return result;
}

char *insert_message(){

	return result;
}*/




