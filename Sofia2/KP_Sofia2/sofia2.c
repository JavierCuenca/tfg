#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include "jsmn.h"
#include "Mensajes.h"
int main() {
	/******* DECLARACION DE LAS VARIABLES****/
	char sendline[4096], recvline[4096];
	int r, pos, tam, n, i;
	char body_message[500], response[1024], sessionkey[50], query[150], querytype[20];
	int msrefresh;
	char json_message[1024];
	char *host = "sofia2.com";
	char *page = "/sib/services/ssap/v01/SSAPResource/";
	char *pagesubs = "/sib/services/ssap/v01/SSAPResource/subscribe"; // quiza la quite y lo haga todo con el query.
	char *pagequery = "/sib/services/ssap/v01/SSAPResource/SQL/";

	/********* Iniciacion de las estructuras  **********/
	struct sockaddr_in cliente;
	struct hostent *servidor; //servidor al que mandamos la informacion 
	servidor = gethostbyname(host); 

	/********** CONFIGURACION DEL SOCKET********/
	int puerto, conexion;
	conexion = socket(AF_INET, SOCK_STREAM, 0); 
	puerto=80;
	bzero((char *)&cliente, sizeof((char *)&cliente));
	cliente.sin_family = AF_INET; 
	cliente.sin_port = htons(puerto); 
	bind((char *)servidor->h_addr, (char *)&cliente.sin_addr.s_addr, sizeof(servidor->h_length));

	/******CONEXION CON EL SOCKET******/
	cliente.sin_addr = *((struct in_addr *)servidor->h_addr); 
	  
	if(connect(conexion,(struct sockaddr *)&cliente, sizeof(cliente)) < 0){ 
		printf("Error conectando con el host\n");
		close(conexion);
		return 1;
	}

	printf("Conectado con %s:%d\n",inet_ntoa(cliente.sin_addr),htons(cliente.sin_port));
	 
	/************ JOIN ************/
	printf("*******************Enviando join***********************\n");
	char instance[100] = "\"AppTermostato1995:AppTermostato199501\""; // Instancia de la ontologia a la que no vamos a conectar 
	char token[100] = "\"ea64c1b18a1b4676a8668ff678f79a87\"";         // Token de la ontologia a la que nos conectamos
	char ontologia[100] = "\"Lectura_Termostato\"";                    // Nombre de la ontologia a la que nos conectamos
	
	strcpy(sendline, join_message(json_message, instance, token, host, page));
	write(conexion, sendline, strlen(sendline));
	printf("*******************Enviando MENSAJE***********************\n");
	
	while ((n = read(conexion, recvline, 4096)) > 0) {
		recvline[n] = '\0';
		printf("%s\n", recvline);
	}

	get_sessionkey(recvline, sessionkey);
	
	strcpy(json_message, "");
	strcpy(sendline, "");
	strcpy(recvline, "");
  	close(conexion);
  	printf("Puesta a cero\n");
  	
  	/**************************GET de prueba****************************/
  	printf("*******************Enviando get***********************\n");
	/********************** Construccion de la estructura de los mensajes body_message y json_message ******************************/
	conexion = socket(AF_INET, SOCK_STREAM, 0);
	if(connect(conexion,(struct sockaddr *)&cliente, sizeof(cliente)) < 0){ 
		printf("Error conectando con el host\n");
		close(conexion);
		return 1;
	}
	strcpy(querytype, "SQLLIKE");
	strcpy(query, "select * from Lectura_Termostato limit 1;"); // limit 1
	
	strcpy(sendline, query_message(json_message, ontologia, sessionkey, query, querytype, host, page));

	write(conexion, sendline, strlen(sendline));
	printf("*******************Enviando MENSAJE***********************\n");
	printf("Esperando respuesta\n"); 
	char recv[4096];

	while ((n = read(conexion, recv, 4096)) > 0) { 
		printf("Esperando respuesta dentro\n"); 
		recv[n] = '\0';
		printf("%s\n", recv);
	}
	printf("Esta es la respuesta: %s\n", recv);
	
	strcpy(json_message, "");
	strcpy(sendline, "");
	strcpy(recvline, "");
  	close(conexion);
  	printf("Puesta a cero\n");
	
	/************ Leave ************/
	printf("SALIENDO\n");
	conexion = socket(AF_INET, SOCK_STREAM, 0);
	if(connect(conexion,(struct sockaddr *)&cliente, sizeof(cliente)) < 0){ 
		printf("Error conectando con el host\n");
		close(conexion);
		return 1;
	}
	strcpy(sendline, leave_message(json_message, sessionkey, host, page));
	
	write(conexion, sendline, strlen(sendline));
	
	while ((n = read(conexion, recvline, 4096)) > 0) {
		recvline[n] = '\0';
		printf("%s\n", recvline);
	}
	
	printf("Cerramos la conexion\n"); 
	close(conexion);
	return 0;
	
}


	
	
	
	
	
	
	