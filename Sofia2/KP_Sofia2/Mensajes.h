#ifndef _MENSAJES_H
#define _MENSAJES_H

int comprobar_json(char *json);

char *join_message(char *json_message, char *instance, char *token, char *host, char *page);

char *leave_message(char *json_message, char *sessionkey, char *host, char *page);

char *query_message(char *json_message, char *ontology, char * sessionkey, char *query, char *querytype, char *host, char *page);

void get_sessionkey(char *message, char *sessionkey);

#endif