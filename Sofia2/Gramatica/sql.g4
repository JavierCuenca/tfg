grammar sql;
/*
* Parser Rules
*/
start			: sql ;

sql  			: select fromsent where? #SentenciaSQL;

select			: ('SELECT' | 'select') clausulaagre #SentenciaSelectAgre
				| ('SELECT' | 'select') clausulasimple #SentenciaSelectSimple 
				;

clausulaagre	: funcionesselect (',' funcionesselect)* ',' windowfun #ClausulaAgreSelect;

clausulasimple	: exprselect (',' exprselect)* #ClausulasimpleSelect
				| '*' #ClausulasimpleSelectAll
				;

funcionesselect : ('MAX' | 'max') '(' exprselect ')' #FuncionesMax
				| ('MIN' | 'min') '(' exprselect ')' #FuncionesMin
				| ('SUM' | 'sum') '(' exprselect ')' #FuncionesSum
				| ('AVG' | 'avg') '(' exprselect ')' #FuncionesAvg
				;

windowfun		: ('WINDOW' | 'window') DATO #FuncionWindow;

exprselect		: DATO #ExprSelect ;

fromsent		: ('FROM' | 'from') DATO #SentenciaFrom;

where 			: ('WHERE' | 'where') (sentencewhere)+ #SentenciaWhere;

sentencewhere	: left=sentencewhere op=('AND' | 'OR' | 'and' | 'or') rigth=sentencewhere #opSentenceWhere
				| exprwhere #ExprWhere
				;

exprwhere 		: left=exprwhere op=('=' | '>' | '<' | '=>' | '<=' | '<>') rigth=exprwhere #opExprWhere
				| '(' exprwhere ')'  #parenExprWhere
				| campo=DATO    #campoExprWhere
				;

/*
* Lexer Rules
*/

fragment A         : [Aa] ;
fragment N         : [Nn] ;
fragment D         : [Dd] ;
fragment O 		   : [Oo] ;
fragment R 		   : [Rr] ;
fragment T 		   : [Tt] ;
fragment M         : [Mm] ;
fragment X         : [Xx] ;
fragment I 		   : [Ii] ;
fragment S 		   : [Ss] ;
fragment U 		   : [Uu] ;
fragment V 		   : [Vv] ;
fragment G 		   : [Gg] ;
fragment F		   : [Ff] ;
fragment E 		   : [Ee] ;
fragment W 		   : [Ww] ;
fragment H 		   : [Hh] ;
fragment L 		   : [Ll] ;
fragment C 		   : [Cc] ;
fragment LOWERCASE : [a-z] ;
fragment UPPERCASE : [A-Z] ;
fragment DIGIT	   : [0-9] ;
fragment DIGITSTRING	   : ('0'..'9') ;

DATO			   : (LOWERCASE | UPPERCASE | DIGIT | '_' | '.')+ ;
NUMBER			   : DIGITSTRING+ ;
K_OPER			   : ('=' | '>' | '<' | '=>' | '<=' | '<>')+ ;
K_LOGIC			   : (AND | OR )+ ;
AND				   : A N D ;
OR				   : O R ;
MAX				   : M A X ;
MIN				   : M I N ;
SUM				   : S U M ;
AVG				   : A V G ;
K_SELECT 		   : S E L E C T ;
K_FROM 			   : F R O M ;
K_WHERE 			   : W H E R E ;
WS    			   : [ \u000B\t\r\n]+ -> skip ;


