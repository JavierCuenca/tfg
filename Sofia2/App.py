#!/usr/bin/python3
import subprocess
import sys
import os

directorio = os.getcwd()
print(directorio)

arg1 = sys.argv[1]
arg2 = sys.argv[2]
arg3 = sys.argv[3]

#Creación de los directorios en los que se generan los archivos a usar.
os.system('mkdir ./Ficheros_Generados/')
os.system('mkdir ./Ficheros_Generados/Gramatica/')
print(os.getcwd())
os.system('mkdir ./Ficheros_Generados/Ontologia/')
os.system('cp ./Generador_CEP/tokenizer.* ./Ficheros_Generados/Ontologia/')

command_line = directorio + '/Ficheros_Generados/Gramatica/:$PYTHONPATHPATH'
os.environ['PYTHONPATH'] = command_line

command_line = 'python3 Compilador.py ' + arg1 + ' ' + arg2
process = subprocess.call(command_line, shell = True)

if process == 0 :
	command_line = 'python3 Ejecutador.py ' + arg3
	process = subprocess.call(command_line, shell = True)
	if process == 0:
		print('\nTodo ha ido bien')



